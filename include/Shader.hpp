#ifndef SHADER_HPP
#define SHADER_HPP

// GLEW includes
#include <GL/glew.h>

// STL includes
#include <string>

class Shader
{
    public:
        Shader(const std::string& vertexFilename, const std::string& fragmentFilename);
        ~Shader();

        void use();

        const GLuint& getProgram();

    private:
        GLuint program;

        std::basic_string<GLchar> fileGetContents(const std::string& filename);
        void compileErrorCheck(GLuint shader);
        void linkErrorCheck();
};

#endif // SHADER_HPP
