#ifndef MODEL_HPP
#define MODEL_HPP

// FBX includes
#include <fbxsdk.h>

// Own includes
#include "Mesh.hpp"

// STL includes
#include <string>
#include <vector>
#include <memory>

class Model
{
    public:
        Model(const std::string& path);

        void draw(Shader &shader);

    private:
        std::vector<std::unique_ptr<Mesh>> meshes;
        std::string directory;
        std::vector<Texture> loadedTextures;

        void load(const std::string& path);
        void processNode(FbxNode* fbxNode);
        void processMesh(FbxMesh* fbxMesh, GLint materialIndex);
        std::vector<Texture> loadMaterialTextures(FbxSurfaceMaterial* material, const char* type, const std::string& typeName);
        GLint loadTextureFromFile(const char* path, const std::string& directory);
};

#endif // MODEL_HPP
