#ifndef MESH_HPP
#define MESH_HPP

// GLEW includes
#include <GL/glew.h>

// GLM includes
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/vec2.hpp>

// STL includes
#include <string>
#include <vector>

class Shader;

struct Vertex {
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec2 texCoord;
};

struct Texture {
    GLuint id;
    std::basic_string<GLchar> type;
    std::string path;
};

class Mesh
{
    public:
        std::vector<Vertex> vertices;
        std::vector<GLuint> indices;
        std::vector<Texture> textures;

        Mesh(const std::vector<Vertex>& vertices, const std::vector<GLuint>& indices, const std::vector<Texture>& textures);
        ~Mesh();

        void draw(Shader& shader);

    private:
        GLuint vao;
        GLuint vbo;
        GLuint ebo;

        void setup();
};

#endif // MESH_HPP
