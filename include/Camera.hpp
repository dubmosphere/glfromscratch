#ifndef CAMERA_HPP
#define CAMERA_HPP

// GLEW includes
#include <GL/glew.h>

// GLM includes
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>

class Camera
{
    public:
        enum Direction {
            FORWARD,
            BACKWARD,
            LEFT,
            RIGHT,
            UP,
            DOWN
        };

        Camera(const glm::vec3& position = glm::vec3(0.0f, 0.0f, 0.0f), const glm::vec3& up = glm::vec3(0.0f, 1.0f, 0.0f), GLfloat yaw = 0.0f, GLfloat pitch = 0.0f);

        glm::mat4 getViewMatrix();
        const glm::vec3 &getPosition();

        void processMovement(Direction direction, GLfloat deltaTime);
        void processRotation(GLfloat xoffset, GLfloat yoffset, GLfloat deltaTime, GLboolean constrainPitch = true);

    private:
        glm::vec3 position;
        glm::vec3 front;
        glm::vec3 up;
        glm::vec3 right;
        glm::vec3 worldUp;

        GLfloat yaw;
        GLfloat pitch;
        GLfloat speed;
        GLfloat sensitivity;

        void updateVectors();
};

#endif // CAMERA_HPP
