// Class includes
#include "Shader.hpp"

// STL includes
#include <fstream>
#include <iostream>
#include <vector>

Shader::Shader(const std::string& vertexFilename, const std::string& fragmentFilename)
{
    std::basic_string<GLchar> vertexCode = fileGetContents(vertexFilename);
    std::basic_string<GLchar> fragmentCode = fileGetContents(fragmentFilename);
    const GLchar* vertexShaderSource = vertexCode.c_str();
    const GLchar* fragmentShaderSource = fragmentCode.c_str();
    GLuint vertexShader;
    GLuint fragmentShader;

    vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
    glCompileShader(vertexShader);
    compileErrorCheck(vertexShader);

    fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
    glCompileShader(fragmentShader);
    compileErrorCheck(fragmentShader);

    program = glCreateProgram();
    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);
    glLinkProgram(program);
    linkErrorCheck();

    glDetachShader(program, fragmentShader);
    glDetachShader(program, vertexShader);

    glDeleteShader(fragmentShader);
    glDeleteShader(vertexShader);
}

Shader::~Shader()
{
    glDeleteProgram(program);
}

const GLuint& Shader::getProgram()
{
    return program;
}

std::basic_string<GLchar> Shader::fileGetContents(const std::string& filename)
{
    std::basic_string<GLchar> content;
    std::basic_ifstream<GLchar> stream(filename, std::ios::in);

    if(stream.is_open()) {
        content = {
            std::istreambuf_iterator<GLchar>(stream),
            std::istreambuf_iterator<GLchar>()
        };
    } else {
        std::cout << "File not found: " << filename << std::endl;
    }

    stream.close();

    return content;
}

void Shader::use()
{
    glUseProgram(program);
}

void Shader::compileErrorCheck(GLuint shader)
{
    GLint success {GL_FALSE};

    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);

    if(!success) {
        GLint infoLogLength {0};
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLength);

        if(infoLogLength > 0) {
            std::vector<GLchar> shaderErrorMessage(infoLogLength + 1);
            glGetShaderInfoLog(shader, infoLogLength, NULL, shaderErrorMessage.data());
            std::cout << shaderErrorMessage.data() << std::endl;
        }
    }
}

void Shader::linkErrorCheck()
{
    GLint success {GL_FALSE};

    glGetProgramiv(program, GL_LINK_STATUS, &success);

    if(!success) {
        GLint infoLogLength {0};
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLength);

        if(infoLogLength > 0) {
            std::vector<GLchar> programErrorMessage(infoLogLength + 1);
            glGetProgramInfoLog(program, infoLogLength, NULL, programErrorMessage.data());
            std::cout << programErrorMessage.data() << std::endl;
        }
    }
}
