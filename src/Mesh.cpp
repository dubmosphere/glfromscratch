// Class includes
#include "Mesh.hpp"

// Own includes
#include "Shader.hpp"

Mesh::Mesh(const std::vector<Vertex>& vertices, const std::vector<GLuint>& indices, const std::vector<Texture>& textures) :
vertices(vertices),
indices(indices),
textures(textures)
{
    setup();
}

Mesh::~Mesh()
{
    for(auto &texture : textures) {
        glDeleteTextures(1, &texture.id);
    }

    glDeleteBuffers(1, &ebo);
    glDeleteBuffers(1, &vbo);
    glDeleteVertexArrays(1, &vao);
}

void Mesh::draw(Shader& shader)
{
    GLuint diffuseNr = 1;
    GLuint specularNr = 1;

    for(GLuint i = 0; i < textures.size(); i++) {
        std::basic_string<GLchar> type = textures.at(i).type;
        std::basic_string<GLchar> number;
        std::basic_string<GLchar> name = "material.";

        if(type == "texture_diffuse") {
            number = std::to_string(diffuseNr++);
        } else if(type == "texture_specular") {
            number = std::to_string(specularNr++);
        }

        name += type + number;

        glActiveTexture(GL_TEXTURE0 + i);
        glUniform1i(glGetUniformLocation(shader.getProgram(), name.c_str()), i);
        glBindTexture(GL_TEXTURE_2D, textures.at(i).id);
    }

    glUniform1f(glGetUniformLocation(shader.getProgram(), "material.shininess"), 16.0f);

    glBindVertexArray(vao);
    glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);

    for(GLuint i = 0; i < textures.size(); i++) {
        glActiveTexture(GL_TEXTURE0 + i);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}

void Mesh::setup()
{
    glGenVertexArrays(1, &vao);
    glGenBuffers(1, &vbo);
    glGenBuffers(1, &ebo);

    glBindVertexArray(vao);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), vertices.data(), GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), indices.data(), GL_STATIC_DRAW);

        // Position
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<GLvoid*>(offsetof(Vertex,position)));

        // Normal
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<GLvoid*>(offsetof(Vertex,normal)));

        // TexCoord
        glEnableVertexAttribArray(2);
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<GLvoid*>(offsetof(Vertex,texCoord)));
    glBindVertexArray(0);
}
