// Class includes
#include "Camera.hpp"

// GLM includes
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

Camera::Camera(const glm::vec3& position, const glm::vec3& up, GLfloat yaw, GLfloat pitch) :
position(position),
front(glm::vec3(0.0f, 0.0f, -1.0f)),
up(up),
right(glm::vec3(1.0f, 0.0f, 0.0f)),
worldUp(up),
yaw(yaw),
pitch(pitch),
speed(5.0f),
sensitivity(5.0f)
{
    updateVectors();
}

glm::mat4 Camera::getViewMatrix()
{
    return glm::lookAt(position, position + front, up);
}

const glm::vec3& Camera::getPosition()
{
    return position;
}

void Camera::processMovement(Direction direction, GLfloat deltaTime)
{
    glm::vec3 movement {0.0f, 0.0f, 0.0f};
    if (direction == FORWARD) {
        movement.x += front.x;
        movement.z += front.z;
    }
    if (direction == BACKWARD) {
        movement.x -= front.x;
        movement.z -= front.z;
    }
    if (direction == LEFT) {
        movement.x -= right.x;
        movement.z -= right.z;
    }
    if (direction == RIGHT) {
        movement.x += right.x;
        movement.z += right.z;
    }

    if(movement != glm::vec3 {0.0f}) {
        movement = glm::normalize(movement);
    }

    // Don't normalize the up movement because we want the full speed in Y direction
    if (direction == UP) {
        movement += worldUp;
    }
    if (direction == DOWN) {
        movement -= worldUp;
    }

    position += movement * speed * deltaTime;
}

void Camera::processRotation(GLfloat xoffset, GLfloat yoffset, GLfloat deltaTime, GLboolean constrainPitch)
{
    xoffset *= sensitivity;
    yoffset *= sensitivity;

    yaw += xoffset * deltaTime;
    pitch += yoffset * deltaTime;

    // Make sure that when pitch is out of bounds, screen doesn't get flipped
    if(constrainPitch) {
        if(pitch > 89.0f) {
            pitch = 89.0f;
        }
        if(pitch < -89.0f) {
            pitch = -89.0f;
        }
    }

    // Update Front, Right and Up Vectors using the updated Eular angles
    updateVectors();
}

void Camera::updateVectors()
{
    front.x = glm::sin(glm::radians(yaw)) * cos(glm::radians(pitch));
    front.y = glm::sin(glm::radians(pitch));
    front.z = -glm::cos(glm::radians(yaw)) * cos(glm::radians(pitch));
    front = glm::normalize(front);

    right = glm::normalize(glm::cross(front, worldUp));
    up = glm::normalize(glm::cross(right, front));
}
