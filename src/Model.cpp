// Class includes
#include "Model.hpp"

// SOIL includes
#include <SOIL/SOIL.h>

// Own includes
#include "Shader.hpp"

// STL includes
#include <iostream>

Model::Model(const std::string& path)
{
    load(path);
}

void Model::draw(Shader& shader)
{
    for(auto &mesh : meshes) {
        mesh.get()->draw(shader);
    }
}

void Model::load(const std::string& path)
{
    FbxManager* fbxManager {FbxManager::Create()};
    FbxIOSettings* fbxIOSettings {FbxIOSettings::Create(fbxManager, IOSROOT)};
    fbxManager->SetIOSettings(fbxIOSettings);

    FbxImporter* fbxImporter {FbxImporter::Create(fbxManager, "fbxImporter")};

    // Initialize the importer.
    GLboolean importStatus {fbxImporter->Initialize(path.c_str(), -1, fbxManager->GetIOSettings())};
    if(!importStatus) {
        std::cout << "Error loading fbx" << std::endl;
    }

    FbxScene* fbxScene {FbxScene::Create(fbxManager, "fbxScene")};
    fbxImporter->Import(fbxScene);
    fbxImporter->Destroy();

    //directory = path.substr(0, path.find_last_of('/'));
    processNode(fbxScene->GetRootNode());

    fbxManager->Destroy();
}

void Model::processNode(FbxNode* fbxNode)
{
    if(fbxNode) {
        for(GLint i {0}; i < fbxNode->GetMaterialCount(); i++) {
            processMesh(fbxNode->GetMesh(), i);
        }

        for(GLint i {0}; i < fbxNode->GetChildCount(); i++) {
            processNode(fbxNode->GetChild(i));
        }
    }
}

void Model::processMesh(FbxMesh* fbxMesh, GLint materialIndex)
{
    if(fbxMesh) {
        std::vector<Vertex> vertices;
        std::vector<GLuint> indices;
        std::vector<Texture> textures;

        if(!fbxMesh->IsTriangleMesh()) {
            FbxGeometryConverter geometryConverter {fbxMesh->GetFbxManager()};
            fbxMesh = static_cast<FbxMesh*>(geometryConverter.Triangulate(fbxMesh, false));
        }

        FbxLayerElementArrayTemplate<FbxVector2>* texCoords {0};
        fbxMesh->GetTextureUV(&texCoords, FbxLayerElement::eTextureDiffuse);

        FbxLayerElementArrayTemplate<GLint>* materialIndices {0};
        fbxMesh->GetMaterialIndices(&materialIndices);

        // Vertices, Normals, texture coords and indices (INDICES NOT WORKING)
        for(GLint i {0}; i < fbxMesh->GetPolygonCount(); i++) {
            if(materialIndices->GetAt(i) == materialIndex) {
                for(GLint j {0}; j < fbxMesh->GetPolygonSize(i); j++) {
                    // Get vertex position
                    FbxVector4 position {fbxMesh->GetControlPointAt(fbxMesh->GetPolygonVertex(i, j))};

                    // Get vertex normal
                    FbxVector4 normal;
                    fbxMesh->GetPolygonVertexNormal(i, j, normal);

                    // Get vertex texture coordinate
                    FbxVector2 texCoord {texCoords->GetAt(fbxMesh->GetTextureUVIndex(i, j, FbxLayerElement::eTextureDiffuse))};

                    // Build vertex
                    Vertex vertex;
                    vertex.position.x = static_cast<GLfloat>(position.mData[0]);
                    vertex.position.y = static_cast<GLfloat>(position.mData[2]);
                    vertex.position.z = static_cast<GLfloat>(position.mData[1]);
                    vertex.normal.x = static_cast<GLfloat>(normal.mData[0]);
                    vertex.normal.y = static_cast<GLfloat>(normal.mData[2]);
                    vertex.normal.z = -static_cast<GLfloat>(normal.mData[1]);
                    vertex.texCoord.x = static_cast<GLfloat>(texCoord.mData[0]);
                    vertex.texCoord.y = 1.0f - static_cast<GLfloat>(texCoord.mData[1]);

                    vertices.push_back(vertex);
                    indices.push_back(vertices.size() - 1); // TODO: Properly index vertices if possible (Or let it be... it works)
                }
            }
        }

        // Textures
        if(materialIndex >= 0) {
            FbxSurfaceMaterial* material {fbxMesh->GetNode()->GetMaterial(materialIndex)};

            std::vector<Texture> diffuseMaps = loadMaterialTextures(material, FbxSurfaceMaterial::sDiffuse, "texture_diffuse");
            textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());

            std::vector<Texture> specularMaps = loadMaterialTextures(material, FbxSurfaceMaterial::sSpecular, "texture_specular");
            textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());

            std::vector<Texture> bumpMaps = loadMaterialTextures(material, FbxSurfaceMaterial::sBump, "texture_bump");
            textures.insert(textures.end(), bumpMaps.begin(), bumpMaps.end());

            std::vector<Texture> normalMaps = loadMaterialTextures(material, FbxSurfaceMaterial::sNormalMap, "texture_normal");
            textures.insert(textures.end(), normalMaps.begin(), normalMaps.end());
        }

        meshes.push_back(std::make_unique<Mesh>(vertices, indices, textures));
    }
}

std::vector<Texture> Model::loadMaterialTextures(FbxSurfaceMaterial* material, const char* type, const std::string& typeName)
{
    std::vector<Texture> textures;
    FbxProperty typeProperty {material->FindProperty(type)};

    for(GLint j = 0; j < typeProperty.GetSrcObjectCount<FbxFileTexture>(); j++) {
        FbxFileTexture *fbxFileTexture {FbxCast<FbxFileTexture>(typeProperty.GetSrcObject<FbxFileTexture>())};
        std::string filename {fbxFileTexture->GetFileName()};
        GLboolean skip {false};

        for(GLuint k {0}; k < loadedTextures.size(); k++) {
            if(loadedTextures[k].path == filename) {
                textures.push_back(loadedTextures[k]);
                skip = true;
                break;
            }
        }

        if(!skip) {
            Texture texture;
            texture.id = loadTextureFromFile(filename.c_str(), directory);
            texture.type = typeName;
            texture.path = filename;

            textures.push_back(texture);
            loadedTextures.push_back(texture);
        }
    }

    return textures;
}

GLint Model::loadTextureFromFile(const char* path, const std::string& directory)
{
    std::string filename {/*directory + '/' + */std::string(path)};

    GLuint id;
    glGenTextures(1, &id);
    glBindTexture(GL_TEXTURE_2D, id);

    int width,height;
    unsigned char* image {SOIL_load_image(filename.c_str(), &width, &height, 0, SOIL_LOAD_RGB)};

    if(!image) {
        std::cout << "Could not load texture: " << filename << std::endl;
    }

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
    glGenerateMipmap(GL_TEXTURE_2D);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_CLAMP);

    SOIL_free_image_data(image);
    glBindTexture(GL_TEXTURE_2D, 0);

    return id;
}
