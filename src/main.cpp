// GLEW includes
#include <GL/glew.h>

// GLFW includes
#include <GLFW/glfw3.h>

// SOIL includes
#include <SOIL/SOIL.h>

// GLM includes
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Own includes
#include "Shader.hpp"
#include "Model.hpp"
#include "Camera.hpp"

// STL includes
#include <vector>
#include <fstream>
#include <iostream>

// Globals
bool keys[1024];
bool mouseButtons[1024];
bool mouseActive = false;

// Callback function prototypes
void keyCallback(GLFWwindow* window, GLint key, GLint scancode, GLint action, GLint mods);
void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
void framebufferSizeCallback(GLFWwindow* window, GLint width, GLint height);

int main(int argc, char** argv)
{
    int windowWidth {800};
    int windowHeight {600};

    glfwInit();

    // Window settings
    glfwWindowHint(GLFW_DEPTH_BITS, 24);
    glfwWindowHint(GLFW_STENCIL_BITS, 8);
    glfwWindowHint(GLFW_SAMPLES, 4);

    // Window and OpenGL context creation
    GLFWwindow* window {glfwCreateWindow(windowWidth, windowHeight, "Hello world", NULL, NULL)};
    glfwSetWindowSizeLimits(window, 400, 300, GLFW_DONT_CARE, GLFW_DONT_CARE);
    glfwMakeContextCurrent(window);

    glewInit(); // Initialize glew immediately after making context current

    glfwSetCursorPos(window, static_cast<double>(windowWidth) / 2.0f, static_cast<double>(windowHeight) / 2.0f);

    // Set event callbacks
    glfwSetKeyCallback(window, keyCallback);
    glfwSetMouseButtonCallback(window, mouseButtonCallback);
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // Enable depth testing
    glEnable(GL_DEPTH_TEST);

    // Enable alpha blending
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // Shader
    Camera camera {glm::vec3 {0.0f, 0.0f, 5.0f}};
    Model block {"models/block.fbx"};
    Model plane {"models/plane.fbx"};
    Model skybox {"models/skybox.fbx"};
    Shader blockShader {"shaders/vertex.vert", "shaders/hue.frag"};
    Shader basicShader {"shaders/vertex.vert", "shaders/fragment.frag"};

    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); // Wireframe mode
    //glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); // Fill mode

    glm::vec3 skyboxPosition {glm::vec3(camera.getPosition().x, camera.getPosition().y, camera.getPosition().z)};

    glm::mat4 blockModel {glm::translate(glm::mat4(), glm::vec3(0.0f,  5.0f, 0.0f))};
    glm::mat4 planeModel {glm::translate(glm::mat4(), glm::vec3(0.0f, -1.0f, 0.0f))};
    glm::mat4 skyboxModel {glm::translate(glm::mat4(),skyboxPosition)};

    glm::mat4 view {camera.getViewMatrix()};
    glm::mat4 projection {glm::perspective(glm::radians(70.0f), static_cast<GLfloat>(windowWidth) / static_cast<GLfloat>(windowHeight), 0.1f, 150.0f)};

    glm::mat4 blockMvp {projection * view * blockModel};
    glm::mat4 planeMvp {projection * view * planeModel};
    glm::mat4 skyboxMvp {projection * view * skyboxModel};

    GLfloat time {0.0f};
    GLfloat hue {0.0f};
    GLfloat saturation {0.5f};
	GLfloat lightness {0.5f};
	GLfloat exposure {2.0f};
	GLfloat gamma {0.5f};
	GLfloat contrast {0.5f};
	GLfloat brightness {0.5f};

    double mousePosX {0.0}, mousePosY {0.0};
    GLfloat mouseOffsetX {0.0f}, mouseOffsetY {0.0f};

    GLfloat startTime {static_cast<GLfloat>(glfwGetTime())};
    GLfloat accumulator {0.0f};
    GLfloat timePerFrame {1.0f / 60.0f};
    GLfloat currentTime {0.0f};
    GLfloat deltaTime {0.0f};

    auto getGlColor = [](GLfloat color) {
        return color / 255.f;
    };

    // Main loop
    while(!glfwWindowShouldClose(window)) {
        while(accumulator >= timePerFrame) {
            glfwPollEvents();

            // Get current window size
            glfwGetWindowSize(window, &windowWidth, &windowHeight);

            if(mouseActive) {
                // Get current mouse position and calculate offset for camera
                glfwGetCursorPos(window, &mousePosX, &mousePosY);
                mouseOffsetX = static_cast<GLfloat>(mousePosX - static_cast<double>(windowWidth) / 2.0);
                mouseOffsetY = static_cast<GLfloat>(static_cast<double>(windowHeight) / 2.0 - mousePosY);  // Reversed since y-coordinates go from bottom to left

                // Reset mouse position to center
                glfwSetCursorPos(window, static_cast<double>(windowWidth) / 2.0, static_cast<double>(windowHeight) / 2.0);

                // Process camera rotation
                camera.processRotation(mouseOffsetX, mouseOffsetY, timePerFrame);
                glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
            } else {
                glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
            }

            if(keys[GLFW_KEY_W]) {
                camera.processMovement(Camera::FORWARD, timePerFrame);
            }
            if(keys[GLFW_KEY_S]) {
                camera.processMovement(Camera::BACKWARD, timePerFrame);
            }
            if(keys[GLFW_KEY_A]) {
                camera.processMovement(Camera::LEFT, timePerFrame);
            }
            if(keys[GLFW_KEY_D]) {
                camera.processMovement(Camera::RIGHT, timePerFrame);
            }
            if(keys[GLFW_KEY_SPACE]) {
                camera.processMovement(Camera::UP, timePerFrame);
            }
            if(keys[GLFW_KEY_LEFT_CONTROL]) {
                camera.processMovement(Camera::DOWN, timePerFrame);
            }

            blockModel = glm::rotate(blockModel, glm::radians(20.0f * timePerFrame), glm::vec3(1.0f, 1.0f, 1.0f));

            glm::vec3 skyboxTranslation {glm::vec3(camera.getPosition().x, camera.getPosition().y, camera.getPosition().z) - skyboxPosition};
            skyboxPosition += skyboxTranslation;
            skyboxModel = glm::translate(skyboxModel, skyboxTranslation);

            view = camera.getViewMatrix();
            blockMvp = projection * view * blockModel;
            skyboxMvp = projection * view * skyboxModel;
            planeMvp = projection * view * planeModel;

            time += static_cast<GLfloat>(timePerFrame);
            hue = time;
            /*saturation = (glm::sin(time) + 1.0f) / 4.0f;
            lightness = (glm::sin(time) + 1.0f) / 4.0f;
            exposure = (glm::sin(time) + 1.0f) / 4.0f;
            gamma = (glm::sin(time) + 1.0f) / 4.0f;
            contrast = (glm::sin(time) + 1.0f) / 4.0f;
            brightness = (glm::sin(time) + 1.0f) / 4.0f;*/

            accumulator -= timePerFrame;
        }

        glClearColor(getGlColor(0.0f), getGlColor(0.0f), getGlColor(0.0f), 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        basicShader.use();
        glUniformMatrix4fv(glGetUniformLocation(basicShader.getProgram(), "modelViewProjection"), 1, GL_FALSE, glm::value_ptr(skyboxMvp));
        glUniform1f(glGetUniformLocation(basicShader.getProgram(), "time"), time);
        skybox.draw(basicShader);
        glUseProgram(0);

        basicShader.use();
        glUniformMatrix4fv(glGetUniformLocation(basicShader.getProgram(), "modelViewProjection"), 1, GL_FALSE, glm::value_ptr(planeMvp));
        glUniform1f(glGetUniformLocation(basicShader.getProgram(), "time"), time);
        plane.draw(basicShader);
        glUseProgram(0);

        blockShader.use();
        glUniformMatrix4fv(glGetUniformLocation(blockShader.getProgram(), "modelViewProjection"), 1, GL_FALSE, glm::value_ptr(blockMvp));
        glUniform1f(glGetUniformLocation(blockShader.getProgram(), "time"), time);
        glUniform1f(glGetUniformLocation(blockShader.getProgram(), "material.hue"), hue);
        glUniform1f(glGetUniformLocation(blockShader.getProgram(), "material.saturation"), saturation);
        glUniform1f(glGetUniformLocation(blockShader.getProgram(), "material.lightness"), lightness);
        glUniform1f(glGetUniformLocation(blockShader.getProgram(), "material.exposure"), exposure);
        glUniform1f(glGetUniformLocation(blockShader.getProgram(), "material.gamma"), gamma);
        glUniform1f(glGetUniformLocation(blockShader.getProgram(), "material.contrast"), contrast);
        glUniform1f(glGetUniformLocation(blockShader.getProgram(), "material.brightness"), brightness);
        block.draw(blockShader);
        glUseProgram(0);

        glfwSwapBuffers(window);

        currentTime = glfwGetTime();
        deltaTime = currentTime - startTime;
        accumulator += deltaTime;
        startTime = currentTime;
    }

    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}

void keyCallback(GLFWwindow* window, GLint key, GLint scancode, GLint action, GLint mods)
{
    if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, GL_TRUE);
    }
    if (key >= 0 && key < 1024) {
        if(action == GLFW_PRESS) {
            keys[key] = true;
        } else if(action == GLFW_RELEASE) {
            keys[key] = false;
        }
    }
}

void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
{
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
        int windowWidth, windowHeight;
        glfwGetWindowSize(window, &windowWidth, &windowHeight);
        glfwSetCursorPos(window, static_cast<double>(windowWidth) / 2.0, static_cast<double>(windowHeight) / 2.0);
        mouseActive = !mouseActive;
    }
    if (button >= 0 && button < 1024) {
        if(action == GLFW_PRESS) {
            mouseButtons[button] = true;
        } else if(action == GLFW_RELEASE) {
            mouseButtons[button] = false;
        }
    }
}

void framebufferSizeCallback(GLFWwindow* window, GLint width, GLint height)
{
    glViewport(0, 0, width, height);
}
