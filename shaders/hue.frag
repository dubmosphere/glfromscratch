/////////////////////////////////////////////////////////////////////////
///////                                                            //////
///////    Hue, Saturation and Lightness Shader by Simon Häsler    //////
///////                                                            //////
/////////////////////////////////////////////////////////////////////////

#version 330 core

// HLSL/Gc -> GLSL
#define saturate(x) clamp(x, 0.0, 1.0)
#define atan2(x, y) atan(y, x)
#define lerp mix
#define fmod mod
#define float2 vec2
#define float3 vec3
#define float4 vec4
#define mat2x2 mat2
#define mat3x3 mat3
#define mat4x4 mat4

// GLSL -> HLSL/Gc
#define atan(y, x) atan2(x, y)
#define mix lerp
#define mod fmod
#define vec2 float2
#define vec3 float3
#define vec4 float4
#define mat2 mat2x2
#define mat3 mat3x3
#define mat4 mat4x4

in vec3 Normal;
in vec2 TexCoord;

out vec4 FragColor;

struct Material {
	float hue;
    float saturation;
	float lightness;
	float exposure;
	float gamma;
	float contrast;
	float brightness;
    sampler2D texture_diffuse1;
};

uniform Material material;
uniform float time;

vec3 rgbToHcv(vec3 rgb);
vec3 rgbToHsl(vec3 rgb);
vec3 hueToRgb(float h);
vec3 hslToRgb(vec3 hsl);

void main()
{
    vec4 fragColor = texture2D(material.texture_diffuse1, TexCoord);
    vec3 hsl = rgbToHsl(fragColor.rgb);

	hsl.x = mod(0.5 * material.hue * 180.0 + hsl.x * 360.0, 360.0) / 360.0;
	hsl.y = saturate(hsl.y + (material.saturation * 2.0 - 1.0));
    hsl.z = saturate(hsl.z + (material.lightness * 2.0 - 1.0));

    fragColor.rgb = hslToRgb(hsl);
    fragColor.rgb = 1 - exp(-fragColor.rgb * material.exposure);
	fragColor.rgb = pow(fragColor.rgb, vec3(1.0 / (material.gamma * 2.0)));
    fragColor.rgb = (fragColor.rgb - 0.5) * max(material.contrast * 2.0, 0.0) + 0.5;
    fragColor.rgb += material.brightness * 2.0 - 1.0;

    FragColor = fragColor;
}

vec3 rgbToHcv(vec3 rgb)
{
	vec4 p = (rgb.g < rgb.b) ? vec4(rgb.bg, -1.0, 2.0 / 3.0) : vec4(rgb.gb, 0.0, -1.0 / 3.0);
	vec4 q = (rgb.r < p.x) ? vec4(p.xyw, rgb.r) : vec4(rgb.r, p.yzx);
	float c = q.x - min(q.w, q.y);
	float h = abs((q.w - q.y) / (6.0 * c + 1e-10) + q.z); // EPSILON = 1e-10

	return vec3(h, c, q.x);
}

vec3 rgbToHsl(vec3 rgb)
{
	vec3 hvc = rgbToHcv(rgb);
	float l = hvc.z - hvc.y * 0.5;
	float s = hvc.y / (1.0 - abs(l * 2.0 - 1.0) + 1e-10); // EPSILON = 1e-10

	return vec3(hvc.x, s, l);
}

vec3 hueToRgb(float h)
{
    return saturate(vec3(abs(h * 6.0 - 3.0) - 1.0, 2.0 - abs(h * 6.0 - 2.0), 2.0 - abs(h * 6.0 - 4.0)));
}

vec3 hslToRgb(vec3 hsl)
{
	vec3 rgb = hueToRgb(hsl.x);
	float c = (1.0 - abs(2.0 * hsl.z - 1.0)) * hsl.y;

	return (rgb - 0.5) * c + hsl.z;
}
