#version 330 core

in vec3 Normal;
in vec2 TexCoord;

out vec4 FragColor;

struct Material {
    float shininess;
    sampler2D texture_diffuse1;
    sampler2D texture_diffuse2;
};

uniform Material material;

void main()
{
    FragColor = mix(texture2D(material.texture_diffuse1, TexCoord), texture2D(material.texture_diffuse2, TexCoord), 0.5);
}
