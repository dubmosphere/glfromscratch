#version 330 core

in vec3 Normal;
in vec2 TexCoord;

out vec4 FragColor;

struct Material {
    float shininess;
    sampler2D texture_diffuse1;
};

uniform Material material;
uniform float time;

void main()
{
    vec4 color = texture2D(material.texture_diffuse1, TexCoord);
    vec3 rgb = color.rgb;
    float maxColor = max(max(rgb.r, rgb.g), rgb.b);
    float minColor = min(min(rgb.r, rgb.g), rgb.b);

    float delta = maxColor - minColor;
    float h, l, s;

    // HSL -> RGB
    if(delta == 0.0) {
        h = 0.0;
    } else if(maxColor == rgb.r) {
        h = mod((rgb.g - rgb.b) / delta, 6.0);
    } else if(maxColor == rgb.g) {
        h = ((rgb.b - rgb.r) / delta) + 2.0;
    } else {
        h = ((rgb.r - rgb.g) / delta) + 4.0;
    }

    h *= 60.f;

    h += (sin(time * 0.5) + 1.0) * 180.0;

    l = (maxColor - minColor) / 2.0;

    if(delta == 0.0) {
        s = 0.0;
    } else if(delta != 0.0) {
        s = delta / (1.0 - abs(2.0 * l - 1.0));
    }

    if(h < 0.0) {
        h = 0.0;
    }
    if(s < 0.0) {
        s=0.0;
    }
    if(l < 0.0) {
        l = 0.0;
    }
    if(h >= 360.0) {
        h = 359.0;
    }
    if(s > 1.0) {
        s = 1.0;
    }
    if(l > 1.0) {
        l = 1.0;
    }

    float hh = h / 60.0;
    float c = (1.0 - abs(2.0 * l - 1.0)) * s;
    float x = c * (1.0 - abs(mod(hh, 2.0) - 1.0));

    rgb = vec3(0.0);

    if(hh >= 0.0 && hh < 1.0) {
        rgb.rg = vec2(c, x);
    } else if(hh >= 1.0 && hh < 2.0) {
        rgb.rg = vec2(x, c);
    } else if(hh >= 2.0 && hh < 3.0) {
        rgb.gb = vec2(c, x);
    } else if(hh >= 3.0 && hh < 4.0) {
        rgb.gb = vec2(x, c);
    } else if(hh >= 4.0 && hh < 5.0) {
        rgb.rb = vec2(x, c);
    } else {
        rgb.rb = vec2(c, x);
    }

    float m = l - (c / 2.0);

    rgb = vec3(rgb.r + m, rgb.g + m, rgb.b + m);

    FragColor = vec4(rgb, color.a);
}
